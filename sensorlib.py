import smbus
import time

REG_STATUS = 0x00
REG_DATA = 0x01
REG_CONFIG = 0x03
REG_ID = 0x11

# Status Register
STATUS_NOT_READY = 0x01

# Config Register
CONFIG_START = 0x01
CONFIG_HEAT = 0x02
CONFIG_HUMIDITY = 0x00
CONFIG_TEMPERATURE = 0x10
CONFIG_FAST = 0x20

# ID Register
ID_SAMPLE = 0xF0
ID_SI7005 = 0x50

# Coefficients
TEMPERATURE_OFFSET = 50
TEMPERATURE_SLOPE = 32
HUMIDITY_OFFSET = 24
HUMIDITY_SLOPE = 16
SCALAR = 16384
A0 = (-78388)  # -4.7844  * SCALAR
A1 = 6567  # 0.4008  * SCALAR
A2 = (-64)  # -0.00393 * SCALAR
Q0 = 3233  # 0.1973  * SCALAR
Q1 = 39  # 0.00237 * SCALAR

si7005_temperature = 25000

bus = smbus.SMBus(1)
address = 0x40


def read_humidity():
    value = measure(CONFIG_HUMIDITY)
    if value < 0:
        return value

    # Convert the humidity to milli-percent (pcm)
    curve = ((value >> 4) * 1000) / HUMIDITY_SLOPE - HUMIDITY_OFFSET * 1000

    # Linearization
    linear = (curve * SCALAR - (curve * curve * A2) / 1000 - curve * A1 - A0 * 1000) / SCALAR

    # Temperature Compensation
    linear = (linear * SCALAR + (si7005_temperature - 30000) * ((linear * Q1) / 1000 + Q0)) / SCALAR

    if linear < 0:
        humidity = 0
    elif linear > 100000:
        humidity = 100000
    else:
        humidity = linear

    return humidity


def read_temperature():
    global si7005_temperature

    value = measure(CONFIG_TEMPERATURE)
    if value < 0:
        return value

    si7005_temperature = (((value >> 2) * 1000) / TEMPERATURE_SLOPE) - (TEMPERATURE_OFFSET * 1000)

    return si7005_temperature


def measure(config):
    error = bus.write_byte_data(address, REG_CONFIG, CONFIG_START | config)
    if error is not None:
        return error

    status = STATUS_NOT_READY
    timeout = time.time() + 10  # 10 seconds from now

    while status and STATUS_NOT_READY and time.time() < timeout:
        status = bus.read_byte_data(address, REG_STATUS)
        if status < 0:
            return status

    error = bus.write_byte_data(address, REG_CONFIG, 0)
    if error is not None:
        return error

    val = bus.read_i2c_block_data(address, 1, 4)
    data = (val[1] << 8) | val[2]

    return data


def init(busnr, addressnr):
    global bus, address
    bus = smbus.SMBus(busnr)
    address = addressnr
