import pygame.camera
import pygame.image
import datetime
import time
import ftplib
import os
import signal
import sys
import ConfigParser

keepRunning = True
configParser = ConfigParser.RawConfigParser()


def signal_handler(signaltype, frame):
    print('You pressed Ctrl+C!')
    global keepRunning
    keepRunning = False


def init():
    # camera initialization
    pygame.camera.init()
    # read config

    configfilepath = r'config'
    configParser.read(configfilepath)
    # bind killsignal
    signal.signal(signal.SIGINT, signal_handler)


def deinit():
    pygame.camera.quit()


def createftpsession():
    # get values from config
    ip = configParser.get('ftp', 'ip')
    username = configParser.get('ftp', 'username')
    password = configParser.get('ftp', 'password')
    # create session
    session = ftplib.FTP(ip, username, password)

    return session


def getdatetime():
    return datetime.datetime.now()


def takepicture(cam, fileinfo):
    # generate filename
    dirname = fileinfo['localdir']
    filename = fileinfo['filename']
    path = dirname + filename

    # save picture
    img = cam.get_image()
    pygame.image.save(img, path)


def ftpfile(session, fileinfo):
    localdir = fileinfo['localdir']
    remotedir = fileinfo['remotedir']
    filename = fileinfo['filename']
    path = localdir + filename

    localfile = open(path, 'rb')
    session.cwd(remotedir)
    session.storbinary('STOR ' + filename, localfile)
    localfile.close()


def rmfile(fileinfo):
    localdir = fileinfo['localdir']
    filename = fileinfo['filename']
    path = localdir + filename

    if os.path.isfile(path):
        os.remove(path)


def createfolders(session):
    dt = getdatetime()

    # load path config
    localpath = configParser.get('folders', 'localpictures')
    remotepath = configParser.get('folders', 'remotepictures')
    folder = dt.strftime("%Y-%m-%d")

    # generate full dir path
    localdir = localpath + folder + '/'
    remotedir = remotepath + folder + '/'

    # generate filename
    filename = dt.strftime("%Y-%m-%d %H-%M-%S") + ".bmp"

    # create local folders
    if not os.path.exists(localdir):
        os.mkdir(localdir)

    # create remote folders
    session.cwd(remotepath)
    if folder not in session.nlst():
        session.mkd(folder)
    session.cwd('/')

    # fill dict with path info
    fileinfo = {'localdir': localdir, 'remotedir': remotedir, 'filename': filename}
    return fileinfo


def main():
    init()

    interval = float(configParser.get('general', 'interval'))
    session = createftpsession()
    cam = pygame.camera.Camera(pygame.camera.list_cameras()[0])
    cam.start()

    # mainloop
    while keepRunning:
        fileinfo = createfolders(session)
        takepicture(cam, fileinfo)
        ftpfile(session, fileinfo)
        rmfile(fileinfo)
        time.sleep(interval)

    deinit()
    sys.exit(0)


if __name__ == "__main__":
    main()